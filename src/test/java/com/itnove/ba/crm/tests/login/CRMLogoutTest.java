package com.itnove.ba.crm.tests.login;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.LoginPage;
import com.itnove.ba.crm.pages.DashboardPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CRMLogoutTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage loginPage;
        DashboardPage dashboardPage;

        driver.navigate().to("http://crm.votarem.lu");

        loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");

        dashboardPage = new DashboardPage(driver);
        dashboardPage.isButtonMenuDisplayed(driver, wait);
        assertTrue(dashboardPage.buttonMenu.getText().contains("SUITECRM DASHBOARD"));

        dashboardPage.logout(hover);


    }
}
