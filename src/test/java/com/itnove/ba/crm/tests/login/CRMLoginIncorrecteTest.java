package com.itnove.ba.crm.tests.login;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;


public class CRMLoginIncorrecteTest extends BaseTest {


    public void checkErrors(String user, String passwd){

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(user, passwd);
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
    }

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://crm.votarem.lu");
        checkErrors("user","nami");
        checkErrors("useer","bitnami");
        checkErrors("uuser","nami");
    }
}


