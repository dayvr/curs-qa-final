package com.itnove.ba.crm.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {

    private WebDriver driver;

    @FindBy(xpath = "id(\"moduleTab_Home\")")
    public WebElement suiteCrmDashboard;

    @FindBy(xpath = "id(\"with-label\")/span[3]")
    public WebElement icono;

    @FindBy(xpath = "id(\"tab0\")")
    public WebElement buttonMenu;

    @FindBy(xpath = "(.//*[@id='logout_link'])[2]")
    public WebElement logout;

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboard));
        return suiteCrmDashboard.isDisplayed();
    }

    public boolean isButtonMenuDisplayed(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(buttonMenu));
        return buttonMenu.isDisplayed();
    }

    public void logout(Actions hover) throws InterruptedException {

        hover.moveToElement(icono).moveToElement(icono)
                .click().build().perform();
        hover.moveToElement(logout).moveToElement(logout)
                .click().build().perform();

    }

    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


}
