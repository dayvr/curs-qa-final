package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "user_name")
    public WebElement usernameTextBox;

    @FindBy(id = "user_password")
    public WebElement userpasswordTextBox;

    @FindBy(xpath = "id(\"bigbutton\")")
    public WebElement botoLogin;

    @FindBy(xpath = "id(\"form\")/span[3]")
    public WebElement warnigText;


    public void login(String user, String passwd){
        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botoLogin.click();
    }

    public boolean isErrorMessagePresent(RemoteWebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(warnigText));
        return warnigText.isDisplayed();
    }


    public LoginPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }



}
