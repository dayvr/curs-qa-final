package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage {

    private WebDriver driver;

    @FindBy(xpath = "id(\"top-links\")/ul[1]/li[5]/a[1]/span[1]")
    public WebElement checkoutButton;


    public CheckoutPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isCheckoutButtonPresent() {

        return checkoutButton.isDisplayed();
    }




}
