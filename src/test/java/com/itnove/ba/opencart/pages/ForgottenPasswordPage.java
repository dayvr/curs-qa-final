package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForgottenPasswordPage {

    @FindBy(xpath = "id(\"input-email\")")
    public WebElement emailTextBox;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/button[1]")
    public WebElement resetButton;


    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]")
    public WebElement errorMessage;

    public void fillAndSendWrongEmail() {
        emailTextBox.clear();
        emailTextBox.sendKeys("dayanis2682@hotmail.com");
        resetButton.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public ForgottenPasswordPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }
}
