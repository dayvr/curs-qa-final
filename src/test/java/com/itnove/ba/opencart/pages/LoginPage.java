package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "input-username")
    public WebElement usernameTextBox;

    @FindBy(id = "input-password")
    public WebElement userpasswordTextBox;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/button[1]")
    public WebElement botoLogin;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]")
    public WebElement errorMessage;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/span[1]/a[1]")
    public WebElement forgottenPassword;

    public void login(String user, String passwd){
        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botoLogin.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public LoginPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

}
