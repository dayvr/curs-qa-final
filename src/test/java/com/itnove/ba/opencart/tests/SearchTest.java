package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.DashboardPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://opencart.votarem.lu");
        DashboardPage searchBoxText = new DashboardPage(driver);
        searchBoxText.searchArticle("MacBook");
        Thread.sleep(3000);
        assertEquals("Macbook", "Macbook");

    }
}
