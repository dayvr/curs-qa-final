package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.pages.DashboardPage;
import com.itnove.ba.opencart.pages.SearchResultsPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AddToCartTestSauce extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://opencart.votarem.lu");
        DashboardPage searchBoxText = new DashboardPage(driver);
        searchBoxText.searchArticle("Apple Cinema");
        Thread.sleep(3000);
        SearchResultsPage resultsPage = new SearchResultsPage(driver);
        resultsPage.results.click();
    }
}
