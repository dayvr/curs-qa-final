package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.get("http://opencart.votarem.lu/admin/index.php");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("useer","bitnami1");
        assertTrue(loginPage.isErrorMessagePresent(driver,wait));

    }
}
