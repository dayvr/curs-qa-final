package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.pages.LoginPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LoginIncorrecteTestSauce extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        System.out.println(driver.getSessionId());
        driver.get("http://opencart.votarem.lu/admin/");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("useer","bitnami1");
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
    }
}
